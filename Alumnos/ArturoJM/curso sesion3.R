library (nycflights13)
filter(flights, month ==12 , day == 1)
arrange(flights, year, month, day)
arrange(flights, desc(dep_delay))
flights %>%
  summarise(
    delay = mean(dep_delay, na.rm = TRUE),
    delay_sd = sd(dep_delay, na.rm = TRUE)
  )
flights %>%
  group_by( year, month, day) %>%
  summarise(delay = mean(dep_delay, na.rm = TRUE))
install.packages("ggplot2")
